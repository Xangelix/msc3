# Modular SC3

A modular rack-like synthesizer built on SuperCollider 3.

## Planned Features

- feat(gui): Column system
- feat(module): Sequence editor, possibly add to envelope module
- feat(module): Scope module
- feat(module): Random number generator module
- feat(server): Use groups/different node stack methods to avoid order of execution issues
- feat(gui,module): Update all module fields to support bus based input

## Known Issues

- fix(module): Pan does not stack correctly

## Usage

1. Tested under SuperCollider 3.11.2
2. Two execution blocks must be run.
3. (To be fixed) Default order of execution settings must be followed (see Docs: Classes | Server > Nodes | Server > Abstractions).
